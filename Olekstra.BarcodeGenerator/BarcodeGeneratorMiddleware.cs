﻿namespace Olekstra.BarcodeGenerator
{
    using System;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using SkiaSharp;
    using ZXing;
    using ZXing.OneD;

    public class BarcodeGeneratorMiddleware
    {
        private static readonly Regex barcodePattern = new Regex(@"^ \d{13} $", RegexOptions.IgnorePatternWhitespace);

        private readonly RequestDelegate _next;

        public BarcodeGeneratorMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public Task Invoke(HttpContext context)
        {
            var barcodeCandidate = context.Request.Path.ToString().Trim('/');

            if (barcodePattern.IsMatch(barcodeCandidate))
            {
                try
                {
                    GenerateBarcode(barcodeCandidate, context.Response);
                }
                catch (Exception ex)
                {
                    context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                    return context.Response.WriteAsync(ex.Message);
                }

                return Task.CompletedTask;
            }

            return _next.Invoke(context);
        }

        public void GenerateBarcode(string barcode, HttpResponse response)
        {
            var writer = new EAN13Writer();
            var matrix = writer.encode(barcode, BarcodeFormat.EAN_13, 500, 300);

            var skWriter = new ZXing.SkiaSharp.BarcodeWriter();
            using (var skBitmap = skWriter.Write(matrix))
            {
                using (var skImage = SKImage.FromBitmap(skBitmap))
                {
                    using (var skData = skImage.Encode())
                    {
                        response.ContentType = "image/png";
                        skData.SaveTo(response.Body);
                    }
                }
            }
        }
    }
}
