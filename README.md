Olekstra Barcode Generator
==========================

Sends PNG image with barcode lines as response for request with EAN13 code in path (e.g. `http://localhost/9780201379631`). 